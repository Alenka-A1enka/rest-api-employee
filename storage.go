package main

import (
	"errors"
	"sync"
)

type Employee struct {
	Id     int    `json:"id"`
	Age    int    `json:"age"`
	Name   string `json:"name"`
	Salary int `json:"salary"`
	// DepartmentId int `json:"department_id"`
}

type Storage interface {
	Insert(e *Employee)
	Update(id int, e Employee)
	Delete(id int)
	Get(id int) (Employee, error)
	GetAll() []Employee
}

type LocalStorage struct {
	coutner int
	data    map[int]Employee
	sync.Mutex
}

func NewLocalStorage() *LocalStorage {
	return &LocalStorage{
		coutner: 1,
		data: make(map[int]Employee),
	}
}

func (s *LocalStorage) Insert(e *Employee) {
	s.Lock()

	e.Id = s.coutner
	s.coutner++

	s.data[e.Id] = *e

	s.Unlock()

}

func (s *LocalStorage) Update(id  int, e Employee) {
	s.Lock()

	s.data[id] = e

	s.Unlock()
}

func (s *LocalStorage) Delete(id  int) {
	s.Lock()

	delete(s.data, id)

	s.Unlock()
}

func (s *LocalStorage) Get(id  int) (Employee, error) {
	s.Lock()
	defer s.Unlock()

	employee, ok := s.data[id]
	if !ok {
		return employee, errors.New("employee not found")
	}
	return employee, nil
}

func (s *LocalStorage) GetAll() []Employee {
	s.Lock()
	defer s.Unlock()

	employees := s.data
	employees_massive := make([]Employee, 0)

	for _, el := range employees {
		employees_massive = append(employees_massive, el)
	}

	return employees_massive
}
