package main

import "github.com/gin-gonic/gin"

func main() {

	storage := NewLocalStorage()
	handler := NewHandler(storage)

	routes := gin.Default()

	routes.POST("/employee", handler.CreateEmployee)
	routes.GET("/employee/:id", handler.GetEmployee)
	routes.GET("/employee", handler.GetAllEmployee)
	routes.PUT("/employee/:id", handler.UpdateEmployee)
	routes.DELETE("/employee/:id", handler.DeleteEmployee)

	routes.Run()

}
