package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ErrorMessage struct {
	Message string `json:"message"`
}

type Handler struct {
	storage Storage
}

func NewHandler(storage Storage) *Handler {
	return &Handler{
		storage: storage,
	}
}

func (h *Handler) CreateEmployee(c *gin.Context) {
	var employee Employee
	if err := c.BindJSON(&employee); err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	h.storage.Insert(&employee)

	c.JSON(http.StatusOK, map[string]interface{} {
		"id": employee.Id,
	})

}

func (h *Handler) GetEmployee(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	employee, err := h.storage.Get(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, employee)

}

func (h *Handler) GetAllEmployee(c *gin.Context) {
	employees := h.storage.GetAll()

	c.JSON(http.StatusOK, employees)
}

func (h *Handler) UpdateEmployee(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	var employee Employee
	if err := c.BindJSON(&employee); err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	h.storage.Update(id, employee)

	c.JSON(http.StatusOK, map[string]interface{} {
		"id": employee.Id,
	})

}

func (h *Handler) DeleteEmployee(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorMessage {
			Message: err.Error(),
		})
		return
	}

	h.storage.Delete(id)

	c.String(http.StatusOK, "employee is deleted")
}
